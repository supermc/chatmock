package com.mac.chatmock.data.db.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.ToOne;

import java.util.Date;
import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.NotNull;

@Entity(nameInDb = "message")
public class Message {
    @Id(autoincrement = true)
    private Long messageId;

    @Property(nameInDb = "msg")
    private String message;

    @Property(nameInDb = "sent_at")
    private Long sentAt;

    @Property(nameInDb = "chatId")
    private long chatId;
    
    @ToOne(joinProperty = "chatId")
    private Chat chat = new Chat();

    @Property(nameInDb = "userId")
    private Long userId;

    private int messageType;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 859287859)
    private transient MessageDao myDao;

    public Long getMessageId() {
        return this.messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getChatId() {
        return this.chatId;
    }

    public void setChatId(long chatId) {
        this.chatId = chatId;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    @Generated(hash = 1528216099)
    private transient Long chat__resolvedKey;

    @Generated(hash = 1217834731)
    public Message(Long messageId, String message, Long sentAt, long chatId, Long userId,
            int messageType) {
        this.messageId = messageId;
        this.message = message;
        this.sentAt = sentAt;
        this.chatId = chatId;
        this.userId = userId;
        this.messageType = messageType;
    }

    @Generated(hash = 637306882)
    public Message() {
    }

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 1098865794)
    public Chat getChat() {
        long __key = this.chatId;
        if (chat__resolvedKey == null || !chat__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            ChatDao targetDao = daoSession.getChatDao();
            Chat chatNew = targetDao.load(__key);
            synchronized (this) {
                chat = chatNew;
                chat__resolvedKey = __key;
            }
        }
        return chat;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 385731932)
    public void setChat(@NotNull Chat chat) {
        if (chat == null) {
            throw new DaoException(
                    "To-one property 'chatId' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.chat = chat;
            chatId = chat.getChatId();
            chat__resolvedKey = chatId;
        }
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getSentAt() {
        return this.sentAt;
    }

    public void setSentAt(Long sentAt) {
        this.sentAt = sentAt;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 747015224)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getMessageDao() : null;
    }

}
