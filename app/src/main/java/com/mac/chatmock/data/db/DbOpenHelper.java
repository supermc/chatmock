package com.mac.chatmock.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.mac.chatmock.data.db.model.DaoMaster;
import com.mac.chatmock.di.ApplicationContext;
import com.mac.chatmock.di.DatabaseInfo;

import javax.inject.Inject;
import javax.inject.Singleton;

import timber.log.Timber;

@Singleton
public class DbOpenHelper extends DaoMaster.OpenHelper {

    @Inject
    public DbOpenHelper(@ApplicationContext Context context, @DatabaseInfo String name) {
        super(context, name);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onUpgrade(db, oldVersion, newVersion);
        Timber.d("DEBUG %s", "DB_OLD_VERSION : " + oldVersion + ", DB_NEW_VERSION : " + newVersion);
    }
}
