package com.mac.chatmock.data.db.repository;

import com.mac.chatmock.data.db.model.Chat;
import com.mac.chatmock.data.db.model.ChatDao;
import com.mac.chatmock.data.db.model.DaoSession;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;


/**
 * This class handles All CRUD related to Chat Database method
 */
public class ChatRepository {

    private final DaoSession mDaoSession;

    @Inject
    public ChatRepository(DaoSession mDaoSession) {
        this.mDaoSession = mDaoSession;
    }

    /**
     * @param chatList list of chats to be inserted
     *
     * Insert chat as a list.
     *
     * @return true if success, false otherwise
     */
    public Observable<Boolean> insertChats(final List<Chat> chatList){
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                mDaoSession.getChatDao().insertInTx(chatList);
                return true;
            }
        });
    }

    /**
     * @param chat new chat object
     *
     * Update current table chat with new data
     *
     * @returnrue if success, false otherwise
     */
    public Observable<Boolean> updateChat(final Chat chat){
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                mDaoSession.getChatDao().update(chat);
                return true;
            }
        });
    }

    /**
     * @return All chats in Table
     */
    public Observable<List<Chat>> getAllChats(){
        return Observable.fromCallable(new Callable<List<Chat>>() {
            @Override
            public List<Chat> call() {
                return mDaoSession.getChatDao().queryBuilder().orderDesc(ChatDao.Properties.TimeStamp).list();
            }
        });
    }
}
