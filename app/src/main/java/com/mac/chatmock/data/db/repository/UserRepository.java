package com.mac.chatmock.data.db.repository;

import com.mac.chatmock.data.db.model.DaoSession;
import com.mac.chatmock.data.db.model.User;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

/**
 * This class handles All CRUD related to Users Database method
 */
public class UserRepository {

    private final DaoSession mDaoSession;

    @Inject
    public UserRepository(DaoSession daoSession) {
        mDaoSession = daoSession;
    }

    /**
     * @return true if empty, false otherwise
     */
    public Observable<Boolean> isUserEmpty() {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return !(mDaoSession.getUserDao().count() > 0);
            }
        });
    }

    /**
     * @param userList list of users to be inserted
     * @return true if inserted successfully, false otherwise
     */
    public Observable<Boolean> saveUserList(final List<User> userList){
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                 mDaoSession.getUserDao().insertInTx(userList);
                 return true;
            }
        });
    }
}
