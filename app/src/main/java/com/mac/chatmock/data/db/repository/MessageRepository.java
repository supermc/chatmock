package com.mac.chatmock.data.db.repository;

import com.mac.chatmock.data.db.model.DaoSession;
import com.mac.chatmock.data.db.model.Message;
import com.mac.chatmock.data.db.model.MessageDao;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;


/**
 * This class handles All CRUD related to Messages Database method
 */
public class MessageRepository {
    private final DaoSession mDaoSession;

    @Inject
    public MessageRepository(DaoSession mDaoSession) {
        this.mDaoSession = mDaoSession;
    }

    /**
     * @param message new message object to be inserted
     *
     * @return last inserted message id
     */
    public Observable<Long> insertMessage(final Message message){
        return Observable.fromCallable(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                return mDaoSession.getMessageDao().insert(message);
            }
        });
    }

    /**
     * @param chatId
     *
     *  Gets all messages where chat.chat_id is equal to messages.chat_id
     *
     * @return list of messages for specific row
     */
    public Observable<List<Message>> getAllMessages(final Long chatId){
        return Observable.fromCallable(new Callable<List<Message>>() {
            @Override
            public List<Message> call() throws Exception {
                final QueryBuilder<Message> messageQueryBuilder = mDaoSession.getMessageDao().queryBuilder().where(MessageDao.Properties.ChatId.eq(chatId));
                return messageQueryBuilder.list();
            }
        });
    }
}
