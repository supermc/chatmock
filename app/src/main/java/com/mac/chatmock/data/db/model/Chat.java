package com.mac.chatmock.data.db.model;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.ToOne;

@Entity(nameInDb = "chat")
public class Chat {
    @Id(autoincrement = true)
    private Long chatId;

    @Property(nameInDb = "users_id")
    private Long usersId;

    @Property(nameInDb = "last_sent_msg")
    private String lastSentMsg;

    @Property(nameInDb = "last_sent_msg_time_stamp")
    private Long timeStamp;

    @ToOne(joinProperty = "usersId")
    private User user = new User();

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 1596497024)
    private transient ChatDao myDao;

    @Generated(hash = 251390918)
    private transient Long user__resolvedKey;


    @Generated(hash = 1780529608)
    public Chat(Long chatId, Long usersId, String lastSentMsg, Long timeStamp) {
        this.chatId = chatId;
        this.usersId = usersId;
        this.lastSentMsg = lastSentMsg;
        this.timeStamp = timeStamp;
    }

    @Generated(hash = 519536279)
    public Chat() {
    }


    public String getLastSentMsg() {
        return lastSentMsg;
    }

    public void setLastSentMsg(String lastSentMsg) {
        this.lastSentMsg = lastSentMsg;
    }
    

    public long getUsersId() {
        return this.usersId;
    }

    public void setUsersId(long usersId) {
        this.usersId = usersId;
    }

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 1895643518)
    public User getUser() {
        Long __key = this.usersId;
        if (user__resolvedKey == null || !user__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            UserDao targetDao = daoSession.getUserDao();
            User userNew = targetDao.load(__key);
            synchronized (this) {
                user = userNew;
                user__resolvedKey = __key;
            }
        }
        return user;
    }



    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 554498850)
    public void setUser(User user) {
        synchronized (this) {
            this.user = user;
            usersId = user == null ? null : user.getId();
            user__resolvedKey = usersId;
        }
    }



    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }





    public void setUsersId(Long usersId) {
        this.usersId = usersId;
    }




    public Long getChatId() {
        return this.chatId;
    }



    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }



    public Long getTimeStamp() {
        return this.timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1004576325)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getChatDao() : null;
    }
}
