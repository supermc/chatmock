package com.mac.chatmock.data.db.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Unique;

@Entity(nameInDb = "users")
public class User {

    @Expose
    @SerializedName("id")
    @Id()
    private Long id;

    @Expose
    @SerializedName("name")
    @Property(nameInDb = "name")
    @Unique
    private String name;

    @Expose
    @SerializedName("profile_pic_image_url")
    @Property(nameInDb = "profile_pic_image_url")
    private String profilePic;


    @Generated(hash = 151189998)
    public User(Long id, String name, String profilePic) {
        this.id = id;
        this.name = name;
        this.profilePic = profilePic;
    }

    @Generated(hash = 586692638)
    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

}
