package com.mac.chatmock.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    public DateUtils() {
    }

    /**
     * @param timeStamp current time in milliseconds
     *
     * @return time in simple formatted date
     */
    public static String formatTimeStamp(Long timeStamp){
        Date date = new Date(timeStamp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss", Locale.getDefault());
        return simpleDateFormat.format(date);
    }
}
