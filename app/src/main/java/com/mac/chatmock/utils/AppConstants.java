package com.mac.chatmock.utils;

public class AppConstants {
    public static final String DB_NAME = "mac_mock_chat.db";

    public static final String SEED_DATABASE_USERS= "seed/users.json";

    public static final Long MY_UNIQUE_ID = -1L;

}
