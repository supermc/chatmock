package com.mac.chatmock.utils;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class FileUtils {
    private FileUtils() {
        // This utility class is not publicly instantiable
    }

    /**
     * This method loads a json file from the asset folder
     *
     * @param context activity context
     * @param jsonFileName file to loaded name
     * @return new UTF-8 string
     * @throws IOException
     */
    public static String loadJSONFromAsset(Context context, String jsonFileName)
            throws IOException {

        AssetManager manager = context.getAssets();
        InputStream is = manager.open(jsonFileName);

        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();

        return new String(buffer, StandardCharsets.UTF_8);
    }
}
