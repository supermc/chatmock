package com.mac.chatmock.ui.splash.dialog;

import com.mac.chatmock.data.db.model.Chat;
import com.mac.chatmock.data.db.model.User;
import com.mac.chatmock.ui.base.ChatMockInteractor;

import java.util.List;

import io.reactivex.Observable;

public interface ChatDialogChatMockInteractor extends ChatMockInteractor {

    Observable<List<Chat>> getAllChats();
}
