package com.mac.chatmock.ui.main;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mac.chatmock.R;
import com.mac.chatmock.data.db.model.Message;
import com.mac.chatmock.ui.base.BaseViewHolder;
import com.mac.chatmock.utils.DateUtils;
import com.mac.chatmock.utils.RoundedImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConversationAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    static final int VIEW_TYPE_MESSAGE_SENT = 1;
    static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;

    private List<Message> messageList;

    public ConversationAdapter(List<Message> messageList) {
        this.messageList = messageList;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_sent, parent, false);
            return new SentMessageHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_received, parent, false);
            return new ReceivedMessageHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder baseViewHolder, int i) {
        baseViewHolder.onBind(i);
    }

    @Override
    public int getItemCount() {
        return messageList != null ? messageList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return messageList.get(position).getMessageType();
    }

    public void addItem(Message message){
        if (message != null) {
            messageList.add(message);
        }
        notifyDataSetChanged();
    }

    public class SentMessageHolder extends BaseViewHolder{
        @BindView(R.id.text_message_body)
        TextView mMessageBodyTv;

        @BindView(R.id.text_message_time)
        TextView mMessageTimeTv;

        public SentMessageHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {
            mMessageBodyTv.setText("");
        }

        public void onBind(int position){
            super.onBind(position);

            Message message = messageList.get(position);

            if (message.getMessage() != null) {
                mMessageBodyTv.setText(message.getMessage());
            }

            mMessageTimeTv.setText(DateUtils.formatTimeStamp(message.getSentAt()));
        }
    }

    public class ReceivedMessageHolder extends BaseViewHolder{

        @BindView(R.id.image_message_profile)
        RoundedImageView mImageView;

        @BindView(R.id.text_message_name)
        TextView mTextMessageUserName;

        @BindView(R.id.text_message_body)
        TextView mMessageBodyTv;

        @BindView(R.id.text_message_time)
        TextView mMessageTimeTv;

        public ReceivedMessageHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {
            mImageView.setImageDrawable(null);
            mTextMessageUserName.setText("");
            mTextMessageUserName.setText("");
        }

        public void onBind(int position){
            super.onBind(position);

            final Message message = messageList.get(position);

            Glide.with(itemView.getContext())
                    .load(R.drawable.ic_account_circle_black_24dp)
                    .asBitmap()
                    .centerCrop()
                    .into(mImageView);

            mTextMessageUserName.setText(message.getChat().getUser().getName());

            mMessageBodyTv.setText(message.getMessage());

            mMessageTimeTv.setText(DateUtils.formatTimeStamp(message.getSentAt()));
        }
    }
}
