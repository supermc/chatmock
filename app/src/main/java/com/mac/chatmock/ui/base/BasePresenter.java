package com.mac.chatmock.ui.base;

import com.mac.chatmock.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Base class that implements the Presenter interface and provides a base implementation for
 * onAttach() and onDetach(). It also handles keeping a reference to the ChatMockView that
 * can be accessed from the children classes by calling getChatMockView().
 */
public class BasePresenter <V extends ChatMockView, I extends ChatMockInteractor>
        implements ChatMockPresenter<V, I> {

    private final SchedulerProvider mSchedulerProvider;
    private final CompositeDisposable mCompositeDisposable;


    private V mChatMockView;
    private I mChatMockInteractor;

    @Inject
    public BasePresenter( I chatMockInteractor,
                          SchedulerProvider schedulerProvider,
                          CompositeDisposable compositeDisposable) {
        mChatMockInteractor = chatMockInteractor;
        mSchedulerProvider = schedulerProvider;
        mCompositeDisposable = compositeDisposable;
    }

    @Override
    public void onAttach(V chatBoxView) {
        mChatMockView = chatBoxView;
    }

    @Override
    public void onDetach() {
        mCompositeDisposable.dispose();
        mChatMockView = null;
        mChatMockInteractor = null;
    }

    @Override
    public V getChatMockView() {
        return mChatMockView;
    }

    @Override
    public I getInteractor() {
        return mChatMockInteractor;
    }

    @Override
    public boolean isViewAttached() {
        return mChatMockView != null;
    }

    public SchedulerProvider getSchedulerProvider() {
        return mSchedulerProvider;
    }

    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }
}
