package com.mac.chatmock.ui.splash.dialog;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mac.chatmock.R;
import com.mac.chatmock.data.db.model.Chat;
import com.mac.chatmock.ui.base.BaseViewHolder;
import com.mac.chatmock.utils.DateUtils;
import com.mac.chatmock.utils.RoundedImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatsAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private LayoutInflater inflater;

    private List<Chat> chatList;

    private Callback callback;

    public ChatsAdapter(List<Chat> chatList) {
        this.chatList = chatList;
    }

    public interface Callback{
        void onChatClick(Chat chat);
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (inflater == null) {
            inflater = LayoutInflater.from(viewGroup.getContext());
        }
        return new ItemViewHolder(inflater.inflate(R.layout.users_item_list, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder baseViewHolder, int i) {
        baseViewHolder.onBind(i);
    }

    @Override
    public int getItemCount() {
        return chatList != null ? chatList.size() : 0;
    }

    public void addItem(List<Chat> list){
        if (list != null){
            chatList.clear();
            chatList.addAll(list);
        }
        notifyDataSetChanged();
    }

    class ItemViewHolder extends BaseViewHolder{
        @BindView(R.id.tv_user_full_name)
        TextView mUserFullNameTextView;

        @BindView(R.id.tv_msg_time_stamp)
        TextView mMsgTimeStampTextView;

        @BindView(R.id.iv_user)
        RoundedImageView mUserRoundedImageView;

        @BindView(R.id.tv_user_msg)
        TextView mUserMsgTextView;


        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {
            mUserRoundedImageView.setImageDrawable(null);
            mUserFullNameTextView.setText("");
            mUserMsgTextView.setText("");
        }

        public void onBind(int position) {
            super.onBind(position);

            final Chat chat = chatList.get(position);

            if (chat.getUser() != null && chat.getUser().getProfilePic() != null) {
                Glide.with(itemView.getContext())
                        .load(chat.getUser().getProfilePic())
                        .asBitmap()
                        .centerCrop()
                        .into(mUserRoundedImageView);
            }

            if (chat.getUser() != null && chat.getUser().getName() != null) {
                mUserFullNameTextView.setText(chat.getUser().getName());
            }

            if (chat.getLastSentMsg() != null) {
                mUserMsgTextView.setText(chat.getLastSentMsg());
                mUserMsgTextView.setVisibility(View.VISIBLE);
            } else {
                mUserMsgTextView.setVisibility(View.GONE);
            }

            if (chat.getTimeStamp() != null) {
                mMsgTimeStampTextView.setText(DateUtils.formatTimeStamp(chat.getTimeStamp()));
                mMsgTimeStampTextView.setVisibility(View.VISIBLE);
            } else {
                mMsgTimeStampTextView.setVisibility(View.GONE);
            }


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callback != null) {
                        callback.onChatClick(chat);
                    }
                }
            });
        }
    }
}
