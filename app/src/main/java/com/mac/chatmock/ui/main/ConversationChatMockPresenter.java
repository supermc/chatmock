package com.mac.chatmock.ui.main;

import com.mac.chatmock.data.db.model.Chat;
import com.mac.chatmock.data.db.model.Message;
import com.mac.chatmock.di.PerActivity;
import com.mac.chatmock.ui.base.ChatMockPresenter;

@PerActivity
public interface ConversationChatMockPresenter<V extends ConversationChatMockView,
        I extends ConversationChatMockInteractor> extends ChatMockPresenter<V, I> {

    void getAllMessages(Long chatId);

    void sendMessage(Message message);

    void updateChat(Chat chat);
}
