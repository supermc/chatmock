package com.mac.chatmock.ui.splash;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.$Gson$Types;
import com.mac.chatmock.data.db.model.Chat;
import com.mac.chatmock.data.db.model.User;
import com.mac.chatmock.data.db.repository.ChatRepository;
import com.mac.chatmock.data.db.repository.UserRepository;
import com.mac.chatmock.di.ApplicationContext;
import com.mac.chatmock.ui.base.BaseInteractor;
import com.mac.chatmock.utils.AppConstants;
import com.mac.chatmock.utils.FileUtils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

public class SplashInteractor extends BaseInteractor implements SplashChatMockInteractor{

    private UserRepository mUserRepository;
    private ChatRepository mChatRepository;
    private Context mContext;

    @Inject
    public SplashInteractor(UserRepository mUserRepository,ChatRepository mChatRepository, @ApplicationContext Context mContext) {
        super();
        this.mUserRepository = mUserRepository;
        this.mChatRepository = mChatRepository;
        this.mContext = mContext;
    }

    @Override
    public Observable<Boolean> seedDataBaseChats() {
        GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
        final Gson gson = builder.create();
        return mUserRepository.isUserEmpty()
                .concatMap(new Function<Boolean, ObservableSource<? extends Boolean>>() {
                    @Override
                    public ObservableSource<? extends Boolean> apply(Boolean isEmpty) throws IOException {
                        if (isEmpty) {
                            Type type = $Gson$Types.newParameterizedTypeWithOwner(
                                    null,
                                    List.class,
                                    User.class);
                            List<User> userList = gson.fromJson(
                                    FileUtils.loadJSONFromAsset(
                                            mContext,
                                            AppConstants.SEED_DATABASE_USERS),
                                    type);
                            mUserRepository.saveUserList(userList);
                            List<Chat> chatList = new ArrayList<>();
                            for (int i = 0; i < userList.size(); i++) {
                                Chat chat = new Chat();
                                chat.setUsersId(userList.get(i).getId());
                                chat.setUser(userList.get(i));
                                chatList.add(chat);
                            }
                            return mChatRepository.insertChats(chatList);
                        }
                        return Observable.just(false);
                    }
                }).concatMap(new Function<Boolean, ObservableSource<? extends Boolean>>() {
                    @Override
                    public ObservableSource<? extends Boolean> apply(Boolean isEmpty) throws Exception {
                        if (isEmpty) {
                            Type type = $Gson$Types.newParameterizedTypeWithOwner(
                                    null,
                                    List.class,
                                    User.class);
                            List<User> userList = gson.fromJson(
                                    FileUtils.loadJSONFromAsset(
                                            mContext,
                                            AppConstants.SEED_DATABASE_USERS),
                                    type);
                            return mUserRepository.saveUserList(userList);
                        }
                        return Observable.just(false);
                    }
                });
    }
}
