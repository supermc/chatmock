package com.mac.chatmock.ui.splash;

import com.mac.chatmock.ui.base.ChatMockView;

public interface SplashChatMockView extends ChatMockView {

    void openChatDialogFragment();

}
