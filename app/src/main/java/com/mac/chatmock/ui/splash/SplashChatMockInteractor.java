package com.mac.chatmock.ui.splash;

import com.mac.chatmock.ui.base.ChatMockInteractor;

import io.reactivex.Observable;

public interface SplashChatMockInteractor extends ChatMockInteractor {

    Observable<Boolean> seedDataBaseChats();
}
