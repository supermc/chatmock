package com.mac.chatmock.ui.splash.dialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.mac.chatmock.R;
import com.mac.chatmock.data.db.model.Chat;
import com.mac.chatmock.di.component.ActivityComponent;
import com.mac.chatmock.ui.base.BaseDialog;
import com.mac.chatmock.ui.main.ConversationActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.mac.chatmock.ui.main.ConversationActivity.EXTRA_USER_NAME;

public class ChatDialog extends BaseDialog implements ChatDialogChatMockView, ChatsAdapter.Callback {

    public static final String TAG = "ChatDialog";

    public static final String EXTRA_CHAT_OBJECT = "EXTRA_CHAT_OBJECT";

    @Inject
    ChatDialogChatMockPresenter<ChatDialogChatMockView, ChatDialogChatMockInteractor> mPresenter;

    @Inject
    ChatsAdapter mChatAdapter;

    @Inject
    LinearLayoutManager mLinearLayoutManager;

    @BindView(R.id.rv_users)
    RecyclerView mRecyclerView;

    public static ChatDialog newInstance() {

        Bundle args = new Bundle();

        ChatDialog fragment = new ChatDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_users_layout, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {

            component.inject(this);

            setUnBinder(ButterKnife.bind(this, rootView));

            mChatAdapter.setCallback(this);

            mPresenter.onAttach(this);
        }
        return rootView;
    }

    @Override
    protected void setUp(View view) {
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getBaseActivity(), DividerItemDecoration.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mChatAdapter);
    }

    @Override
    public void loadChats(List<Chat> chatList) {
        mChatAdapter.addItem(chatList);
    }

    @Override
    public void onChatClick(Chat chat) {
        if (chat != null) {
            Intent intent = ConversationActivity.getStartIntent(getBaseActivity());
            intent.putExtra(EXTRA_CHAT_OBJECT, new Gson().toJson(chat));
            intent.putExtra(EXTRA_USER_NAME, chat.getUser().getName());
            startActivity(intent);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        getBaseActivity().finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onViewInitialized();
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }
}
