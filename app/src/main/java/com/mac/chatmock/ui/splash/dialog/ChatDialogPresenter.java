package com.mac.chatmock.ui.splash.dialog;

import com.mac.chatmock.R;
import com.mac.chatmock.data.db.model.Chat;
import com.mac.chatmock.ui.base.BasePresenter;
import com.mac.chatmock.utils.rx.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import timber.log.Timber;

public class ChatDialogPresenter<V extends ChatDialogChatMockView, I extends ChatDialogChatMockInteractor>
        extends BasePresenter<V, I> implements ChatDialogChatMockPresenter<V,I> {

    @Inject
    public ChatDialogPresenter(I chatMockInteractor,
                               SchedulerProvider schedulerProvider,
                               CompositeDisposable compositeDisposable) {
        super(chatMockInteractor, schedulerProvider, compositeDisposable);
    }

    /**
     * get database chats activity is created
     */
    @Override
    public void onViewInitialized() {

        getChatMockView().showLoading();

        getCompositeDisposable().add(getInteractor().getAllChats()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<List<Chat>>() {
                    @Override
                    public void accept(List<Chat> userList) {
                        if (!isViewAttached()) {
                            return;
                        }

                        if (userList != null) {
                            getChatMockView().loadChats(userList);
                        }

                        getChatMockView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        Timber.e("Error while getting list of users from database : %s", throwable.getMessage());

                        if (!isViewAttached()) {
                            return;
                        }

                        getChatMockView().hideLoading();

                        getChatMockView().onError(R.string.some_error);
                    }
                })
        );

    }
}
