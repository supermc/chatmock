package com.mac.chatmock.ui.main;

import com.mac.chatmock.data.db.model.Message;
import com.mac.chatmock.ui.base.ChatMockView;

import java.util.List;

public interface ConversationChatMockView extends ChatMockView {
    void loadMessages(List<Message> messageList);
}
