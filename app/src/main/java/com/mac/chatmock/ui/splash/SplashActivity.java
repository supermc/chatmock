package com.mac.chatmock.ui.splash;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.mac.chatmock.R;
import com.mac.chatmock.ui.base.BaseActivity;
import com.mac.chatmock.ui.splash.dialog.ChatDialog;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity implements SplashChatMockView {

    @Inject
    SplashChatMockPresenter<SplashChatMockView, SplashChatMockInteractor> mPresenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(SplashActivity.this);
    }

    @Override
    public void openChatDialogFragment() {
        ChatDialog.newInstance().show(getSupportFragmentManager(), ChatDialog.TAG);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {

    }
}
