package com.mac.chatmock.ui.main;

import com.mac.chatmock.data.db.model.Chat;
import com.mac.chatmock.data.db.model.Message;
import com.mac.chatmock.data.db.repository.ChatRepository;
import com.mac.chatmock.data.db.repository.MessageRepository;
import com.mac.chatmock.ui.base.BaseInteractor;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class ConversationInteractor extends BaseInteractor implements ConversationChatMockInteractor {

    private MessageRepository mMessageRepository;
    private ChatRepository mChatRepository;

    @Inject
    public ConversationInteractor(MessageRepository mMessageRepository, ChatRepository mChatRepository) {
        this.mMessageRepository = mMessageRepository;
        this.mChatRepository = mChatRepository;
    }


    @Override
    public Observable<List<Message>> getAllMessages(long chatId) {
        return mMessageRepository.getAllMessages(chatId);
    }

    @Override
    public Observable<Long> insertNewMessage(Message message) {
        return mMessageRepository.insertMessage(message);
    }

    @Override
    public Observable<Boolean> updateChat(Chat chat) {
        return mChatRepository.updateChat(chat);
    }
}
