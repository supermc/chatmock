package com.mac.chatmock.ui.splash;

import com.mac.chatmock.R;
import com.mac.chatmock.ui.base.BasePresenter;
import com.mac.chatmock.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import timber.log.Timber;

public class SplashPresenter <V extends SplashChatMockView, I extends SplashChatMockInteractor>
        extends BasePresenter<V, I> implements SplashChatMockPresenter<V, I> {


    @Inject
    public SplashPresenter(I chatMockInteractor,
                           SchedulerProvider schedulerProvider,
                           CompositeDisposable compositeDisposable) {
        super(chatMockInteractor, schedulerProvider, compositeDisposable);
    }

    /**
     * @param chatBoxView presenter attached to view (activity)
     *
     * seed database chats when user launches app , this is called on Activity's onCreate()
     */
    @Override
    public void onAttach(V chatBoxView) {
        super.onAttach(chatBoxView);
        getCompositeDisposable().add(getInteractor()
                .seedDataBaseChats()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }
                        getChatMockView().hideLoading();
                        openMyContactsDialog();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Timber.e("Error while seeding users : %s", throwable.getMessage());
                        if (!isViewAttached()) {
                            return;
                        }

                        getChatMockView().hideLoading();
                        getChatMockView().onError(R.string.some_error);
                    }
                }));
    }

    /**
     * Open my contacts chat dialog
     */
    private void openMyContactsDialog(){
        getChatMockView().openChatDialogFragment();
    }
}
