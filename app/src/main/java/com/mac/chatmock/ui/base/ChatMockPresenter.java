package com.mac.chatmock.ui.base;

/**
 * Every presenter in the app must either implement this interface or extend BasePresenter
 * indicating the ChatMockView type that wants to be attached with.
 */
public interface ChatMockPresenter <V extends ChatMockView, I extends ChatMockInteractor>{

    void onAttach(V ChatBoxView);

    void onDetach();

    V getChatMockView();

    I getInteractor();

    boolean isViewAttached();
}
