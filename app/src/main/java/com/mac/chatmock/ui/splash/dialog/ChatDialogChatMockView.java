package com.mac.chatmock.ui.splash.dialog;

import com.mac.chatmock.data.db.model.Chat;
import com.mac.chatmock.data.db.model.User;
import com.mac.chatmock.ui.base.DialogChatMockView;

import java.util.List;

public interface ChatDialogChatMockView extends DialogChatMockView {

    void loadChats(List<Chat> chatList);
}
