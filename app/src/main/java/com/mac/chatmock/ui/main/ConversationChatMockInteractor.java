package com.mac.chatmock.ui.main;

import com.mac.chatmock.data.db.model.Chat;
import com.mac.chatmock.data.db.model.Message;
import com.mac.chatmock.ui.base.ChatMockInteractor;

import java.util.List;

import io.reactivex.Observable;

public interface ConversationChatMockInteractor extends ChatMockInteractor {

    Observable<List<Message>> getAllMessages(long chatId);

    Observable<Long> insertNewMessage(Message message);

    Observable<Boolean> updateChat(Chat chat);
}
