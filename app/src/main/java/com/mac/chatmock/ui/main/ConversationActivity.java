package com.mac.chatmock.ui.main;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mac.chatmock.R;
import com.mac.chatmock.data.db.model.Chat;
import com.mac.chatmock.data.db.model.Message;
import com.mac.chatmock.ui.base.BaseActivity;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.mac.chatmock.ui.main.ConversationAdapter.VIEW_TYPE_MESSAGE_RECEIVED;
import static com.mac.chatmock.ui.main.ConversationAdapter.VIEW_TYPE_MESSAGE_SENT;
import static com.mac.chatmock.ui.splash.dialog.ChatDialog.EXTRA_CHAT_OBJECT;
import static com.mac.chatmock.utils.AppConstants.MY_UNIQUE_ID;


public class ConversationActivity extends BaseActivity implements ConversationChatMockView {

    public static final String EXTRA_USER_NAME = "EXTRA_USER_NAME";

    @Inject
    ConversationChatMockPresenter<ConversationChatMockView, ConversationChatMockInteractor> mPresenter;

    @BindView(R.id.rv_message_list)
    RecyclerView mRecycler;

    @BindView(R.id.edittext_chatbox)
    EditText mChatBoxEt;

    @Inject
    ConversationAdapter adapter;

    @Inject
    LinearLayoutManager mLinearLayoutManager;

    private Handler handler = new Handler();

    /**
     * @param context Activity context
     * @return new intent of this activity
     */
    public static Intent getStartIntent(Context context) {
        return new Intent(context, ConversationActivity.class);
    }

    /**
     * @return selected chat object
     */
    private Chat getChatFromIntent() {
        Chat chat = new Chat();
        Intent intent = getIntent();
        if (intent != null) {
            String strObj = getIntent().getStringExtra(EXTRA_CHAT_OBJECT);
            chat = new Gson().fromJson(strObj, Chat.class);
        }
        return chat;
    }

    /**
     * @return selected user name
     */
    private String getUserNameFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            return intent.getStringExtra(EXTRA_USER_NAME);
        }
        return "";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(this);

        setUp();

        mPresenter.getAllMessages(getChatFromIntent().getChatId());
    }

    @OnClick(R.id.button_chatbox_send)
    void onSendMessageClick(View v) {
        onSendButtonClick();
    }

    @Override
    protected void setUp() {
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getUserNameFromIntent());
        mRecycler.setLayoutManager(mLinearLayoutManager);
        mRecycler.setHasFixedSize(false);
        mRecycler.setItemAnimator(new DefaultItemAnimator());
        mRecycler.setAdapter(adapter);

        // handle keyboard send button
        mChatBoxEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_SEND)) {
                    onSendButtonClick();
                }
                return false;
            }
        });
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        handler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }

    @Override
    public void loadMessages(List<Message> messageList) {
        // loop over messages and set message type according to sender / receiver
        for (int i = 0; i < messageList.size(); i++) {
            final Message message = messageList.get(i);
            if (message.getUserId().equals(MY_UNIQUE_ID)) {
                message.setMessageType(VIEW_TYPE_MESSAGE_SENT);
            } else {
                message.setMessageType(VIEW_TYPE_MESSAGE_RECEIVED);
            }
            adapter.addItem(message);
        }
        mLinearLayoutManager.scrollToPosition(messageList.size() - 1);
    }

    /**
     * This method handles send button click
     */
    private void onSendButtonClick() {
        // get text from edit text
        final String messageStr = mChatBoxEt.getText().toString();
        // get chat object from intent
        final Chat chat = getChatFromIntent();
        // check if edit text input if not empty
        if (!TextUtils.isEmpty(messageStr)) {
            // send message
            sendMyMessage(chat, messageStr);
            // clear edit text
            mChatBoxEt.getText().clear();
            // delay posting receiver message for 2 secs
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    sendReceiverMessage(chat, messageStr);
                }
            }, 2000);
            // scroll adapter to last item
            mLinearLayoutManager.scrollToPosition(adapter.getItemCount() - 1);
        }
    }

    /**
     * This method handles sending messages of current user
     *
     * @param chat       object from DB
     * @param messageStr message inputted from edit text
     */
    private void sendMyMessage(Chat chat, String messageStr) {
        final Message message = new Message();
        message.setChat(chat);
        message.setChatId(chat.getChatId());
        final long timeInMillis = Calendar.getInstance().getTimeInMillis();
        message.setSentAt(timeInMillis);
        message.setMessage(messageStr);
        message.setMessageType(VIEW_TYPE_MESSAGE_SENT);
        chat.setLastSentMsg(messageStr);
        chat.setTimeStamp(timeInMillis);
        message.setUserId(MY_UNIQUE_ID);
        mPresenter.updateChat(chat);
        mPresenter.sendMessage(message);
        adapter.addItem(message);
    }

    /**
     * This method handles receiving current user messages and re sending same (for mocking purposes)
     *
     * @param chat       object from DB
     * @param messageStr message inputted from edit text
     */
    private void sendReceiverMessage(Chat chat, String messageStr) {
        Message message1 = new Message();
        message1.setUserId(chat.getUsersId());
        message1.setChat(chat);
        final long timeInMillis = Calendar.getInstance().getTimeInMillis();
        message1.setSentAt(timeInMillis);
        message1.setChatId(chat.getChatId());
        message1.setMessage(messageStr);
        message1.setMessageType(VIEW_TYPE_MESSAGE_RECEIVED);
        chat.setLastSentMsg(messageStr);
        chat.setTimeStamp(timeInMillis);
        mPresenter.updateChat(chat);
        mPresenter.sendMessage(message1);
        adapter.addItem(message1);

        mLinearLayoutManager.scrollToPosition(adapter.getItemCount() - 1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
