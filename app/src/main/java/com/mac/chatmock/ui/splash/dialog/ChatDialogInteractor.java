package com.mac.chatmock.ui.splash.dialog;

import com.mac.chatmock.data.db.model.Chat;
import com.mac.chatmock.data.db.repository.ChatRepository;
import com.mac.chatmock.ui.base.BaseInteractor;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class ChatDialogInteractor extends BaseInteractor implements ChatDialogChatMockInteractor {

    private ChatRepository mChatRepository;

    @Inject
    public ChatDialogInteractor(ChatRepository mChatRepository) {
        this.mChatRepository = mChatRepository;
    }

    @Override
    public Observable<List<Chat>> getAllChats() {
        return mChatRepository.getAllChats();
    }
}
