package com.mac.chatmock.ui.splash;

import com.mac.chatmock.di.PerActivity;
import com.mac.chatmock.ui.base.ChatMockPresenter;

@PerActivity
public interface SplashChatMockPresenter <V extends SplashChatMockView,
        I extends SplashChatMockInteractor> extends ChatMockPresenter<V, I> {
}
