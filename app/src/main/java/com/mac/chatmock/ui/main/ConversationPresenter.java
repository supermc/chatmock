package com.mac.chatmock.ui.main;

import com.mac.chatmock.R;
import com.mac.chatmock.data.db.model.Chat;
import com.mac.chatmock.data.db.model.Message;
import com.mac.chatmock.ui.base.BasePresenter;
import com.mac.chatmock.utils.rx.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import timber.log.Timber;

public class ConversationPresenter <V extends ConversationChatMockView, I extends ConversationChatMockInteractor>
        extends BasePresenter<V,I> implements ConversationChatMockPresenter<V, I>{

    @Inject
    public ConversationPresenter(I chatMockInteractor,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable) {
        super(chatMockInteractor, schedulerProvider, compositeDisposable);
    }

    /**
     * @param chatId
     *
     * Get all messages
     */
    @Override
    public void getAllMessages(Long chatId) {
        getCompositeDisposable().add(getInteractor().getAllMessages(chatId)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        .subscribe(new Consumer<List<Message>>() {
            @Override
            public void accept(List<Message> messages) throws Exception {
                Timber.e("getting messages : %s", messages);

                if (!isViewAttached()) {
                    return;
                }

                getChatMockView().loadMessages(messages);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                Timber.e("Error while getting messages : %s", throwable.getMessage());
            }
        }));
    }

    /**
     * @param message object to be sent
     *
     * Send message to insert into messages table
     */
    @Override
    public void sendMessage(Message message) {

        getCompositeDisposable().add(getInteractor().insertNewMessage(message)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) {
                        if (!isViewAttached()){
                            return;
                        }

                        Timber.e("New Message added : %s", aLong);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        Timber.e("Error while inserting message : %s", throwable.getMessage());

                        if (!isViewAttached()) {
                            return;
                        }

                        getChatMockView().onError(R.string.some_error);
                    }
                }));

    }

    /**
     * @param chat object to update current chat row
     */
    @Override
    public void updateChat(Chat chat) {
        getCompositeDisposable().add(getInteractor().updateChat(chat)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        .subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean aBoolean) throws Exception {

            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                Timber.e("Error while updating chat : %s", throwable.getMessage());
            }
        }));
    }
}
