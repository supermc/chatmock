package com.mac.chatmock.ui.splash.dialog;

import com.mac.chatmock.di.PerActivity;
import com.mac.chatmock.ui.base.ChatMockPresenter;

@PerActivity
public interface ChatDialogChatMockPresenter<V extends ChatDialogChatMockView,
        I extends ChatDialogChatMockInteractor> extends ChatMockPresenter<V, I> {

    void onViewInitialized();
}
