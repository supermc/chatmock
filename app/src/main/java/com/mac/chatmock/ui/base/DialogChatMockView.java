package com.mac.chatmock.ui.base;

public interface DialogChatMockView extends ChatMockView {

    void dismissDialog();
}
