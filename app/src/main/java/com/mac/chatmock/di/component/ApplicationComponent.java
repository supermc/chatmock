package com.mac.chatmock.di.component;

import android.app.Application;
import android.content.Context;

import com.mac.chatmock.ChatMockApp;
import com.mac.chatmock.data.db.model.DaoSession;
import com.mac.chatmock.di.ApplicationContext;
import com.mac.chatmock.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(ChatMockApp mockApp);

    @ApplicationContext
    Context context();

    Application application();

    DaoSession daoSession();
}