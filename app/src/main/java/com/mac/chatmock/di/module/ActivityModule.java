package com.mac.chatmock.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.mac.chatmock.data.db.model.Chat;
import com.mac.chatmock.data.db.model.Message;
import com.mac.chatmock.di.ActivityContext;
import com.mac.chatmock.di.PerActivity;
import com.mac.chatmock.ui.main.ConversationAdapter;
import com.mac.chatmock.ui.main.ConversationChatMockInteractor;
import com.mac.chatmock.ui.main.ConversationChatMockPresenter;
import com.mac.chatmock.ui.main.ConversationChatMockView;
import com.mac.chatmock.ui.main.ConversationInteractor;
import com.mac.chatmock.ui.main.ConversationPresenter;
import com.mac.chatmock.ui.splash.SplashChatMockInteractor;
import com.mac.chatmock.ui.splash.SplashChatMockPresenter;
import com.mac.chatmock.ui.splash.SplashChatMockView;
import com.mac.chatmock.ui.splash.SplashInteractor;
import com.mac.chatmock.ui.splash.SplashPresenter;
import com.mac.chatmock.ui.splash.dialog.ChatDialogChatMockInteractor;
import com.mac.chatmock.ui.splash.dialog.ChatDialogChatMockPresenter;
import com.mac.chatmock.ui.splash.dialog.ChatDialogChatMockView;
import com.mac.chatmock.ui.splash.dialog.ChatDialogPresenter;
import com.mac.chatmock.ui.splash.dialog.ChatsAdapter;
import com.mac.chatmock.ui.splash.dialog.ChatDialogInteractor;
import com.mac.chatmock.utils.rx.AppSchedulerProvider;
import com.mac.chatmock.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class ActivityModule {
    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    @PerActivity
    SplashChatMockPresenter<SplashChatMockView, SplashChatMockInteractor> provideSplashPresenter(
            SplashPresenter<SplashChatMockView, SplashChatMockInteractor> presenter) {
        return presenter;
    }


    @Provides
    @PerActivity
    ChatDialogChatMockPresenter<ChatDialogChatMockView, ChatDialogChatMockInteractor> provideChatDialogPresenter(
            ChatDialogPresenter<ChatDialogChatMockView, ChatDialogChatMockInteractor> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    ConversationChatMockPresenter<ConversationChatMockView, ConversationChatMockInteractor> provideConversationPresenter(
            ConversationPresenter<ConversationChatMockView,ConversationChatMockInteractor>presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    SplashChatMockInteractor provideSplashChatMockInteractor(SplashInteractor interactor) {
        return interactor;
    }

    @Provides
    @PerActivity
    ChatDialogChatMockInteractor provideChatDialogChatMockInteractor(ChatDialogInteractor interactor){
        return interactor;
    }

    @Provides
    @PerActivity
    ConversationChatMockInteractor provideConversationChatMockInteractor(ConversationInteractor interactor){
        return interactor;
    }

    @Provides
    ChatsAdapter provideUserAdapter(){
        return new ChatsAdapter(new ArrayList<Chat>());
    }

    @Provides
    ConversationAdapter provideConversationAdapter(){
        return new ConversationAdapter(new ArrayList<Message>());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }

}
