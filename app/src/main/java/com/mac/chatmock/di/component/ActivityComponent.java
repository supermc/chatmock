package com.mac.chatmock.di.component;

import com.mac.chatmock.ui.main.ConversationActivity;
import com.mac.chatmock.di.PerActivity;
import com.mac.chatmock.di.module.ActivityModule;
import com.mac.chatmock.ui.splash.SplashActivity;
import com.mac.chatmock.ui.splash.dialog.ChatDialog;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(SplashActivity splashActivity);

    void inject(ChatDialog usersDialog);

    void inject(ConversationActivity conversationActivity);
}
