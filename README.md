# ChatMock

This repository breaks the Datamanger into interactors and DbHelper into repositories. These changes makes it fit for a very large projects. It contains detailed chat mock app that implements MVP enahnced with Interactors and DbRepositories for complete decoupling, using Dagger2, GreenDao, RxJava and AndroidDebugDatabase.

# Project Structure
![Structure](https://i.ibb.co/zspYkFX/structure.png)

#### The app has following packages:
1. **data**: It contains all the data accessing and manipulating components.
2. **di**: Dependency providing classes using Dagger2.
3. **ui**: View classes along with their corresponding Presenters and Interactors.
4. **utils**: Utility classes.


#### Classes have been designed in such a way that it could be inherited and maximize the code reuse.


### License
```
   Copyright (C) 2019 Supermac

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```